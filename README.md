# Courses Application

Il s'agit d'un mini projet fait en React + Vite afin de lister simplement des cours avec un style particulier.

## Installation

```npm install```

## Lancement

```npm run dev```

## Build

```npm run build```

## Comment contribuer ?

Pour contribuer, il suffit de faire un fork du projet, de faire vos modifications et de faire une pull request.