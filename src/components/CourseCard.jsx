import {LuTimer, LuListVideo } from "react-icons/lu";
const CourseCard = ({course}) => {

    return (<article className="course-item">
        <div className="course-item-body">
            <div className="course-icons">
                <img src={course?.imgSrc} alt={`${course?.title} logo`} className="course-icon"/>
            </div>
            <h3 className="course-item-title">
                <a href={course?.link} target="_blank" rel="noreferrer">{course?.title}</a>
            </h3>
            <div className="course-item-description">
                <p>{course?.description}</p>
            </div>
        </div>
        <footer className="course-item-footer">
            {course?.playlist_name ?
                <span className="course-author">
                    <LuListVideo/>
                    <span>{course?.playlist_name}</span>
                </span>
                :
                <span className="course-author">
                </span>
            }
            <span className="course-time">
                <LuTimer/>
                <span>{course?.time}</span>
            </span>
        </footer>
    </article>);
}

export default CourseCard;