import {LuVideo} from "react-icons/lu";

const Hero = () => {
    return (
        <section id="hero" className="relative overflow-hidden before:absolute before:top-0 before:left-1/2 before:bg-[url('../svg/component/polygon.svg')] before:bg-no-repeat before:bg-top before:bg-cover before:w-full before:h-full before:-z-[1] before:transform before:-translate-x-1/2 dark:before:bg-[url('../svg/component/polygon-dark.svg')]">
            <div className="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8 pt-24 pb-10">
                <div className="flex justify-center">
                    <a className="inline-flex items-center gap-x-2 bg-white border border-gray-200 text-sm text-gray-800 p-1 pl-3 rounded-full transition hover:border-gray-300 dark:bg-gray-800 dark:border-gray-700 dark:hover:border-gray-600 dark:text-gray-200" target="_blank" href="https://lazarefortune.com/#contact">
                        Devenir un de nos formateurs ?
                        <span className="py-2 px-3 inline-flex justify-center items-center gap-x-2 rounded-full bg-gray-200 font-semibold text-sm text-gray-600 dark:bg-gray-700 dark:text-gray-400">
                          <svg className="w-2.5 h-2.5" width="16" height="16" viewBox="0 0 16 16" fill="none">
                            <path d="M5.27921 2L10.9257 7.64645C11.1209 7.84171 11.1209 8.15829 10.9257 8.35355L5.27921 14" stroke="currentColor" strokeWidth="2" strokeLinecap="round"/>
                          </svg>
                        </span>
                    </a>
                </div>

                <div className="mt-5 max-w-3xl text-center mx-auto">
                    <h1 className="block font-bold text-gray-800 text-4xl md:text-5xl lg:text-6xl dark:text-gray-200">
                        <span>Des contenus </span>
                        <span className="bg-clip-text bg-gradient-to-tl from-yellow-600 to-slate-600 text-transparent">100% gratuits</span>
                    </h1>
                </div>

                <div className="mt-5 max-w-3xl text-center mx-auto">
                    <p className="text-lg text-gray-600 dark:text-gray-400">
                        Vous souhaitez vous former au développement web ? Vous êtes au bon endroit ! Nous vous proposons des formations gratuites pour apprendre à coder.
                    </p>
                </div>

                <div className="mt-8 grid gap-3 w-full sm:inline-flex sm:justify-center">
                    <a className="button-primary button-lg flex justify-center items-center gap-2" href="#courses-list">
                        <LuVideo className="w-5 h-5"/>
                        <span>Commencer</span>
                    </a>
                </div>
            </div>
        </section>
    )
}
export default Hero;