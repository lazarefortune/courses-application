import CourseCard from "./CourseCard.jsx";

function CoursesList({ courses }) {
  return (
    <section id="courses-list">
      <h2 className="text-3xl font-bold mt-2 mb-4">Liste des cours</h2>
      <div className="courses-list">
        {courses.map((course, index) => (
          <CourseCard key={index} course={course} />
        ))}
      </div>
    </section>
  );
}

export default CoursesList;
