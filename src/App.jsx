import './App.css';
import courses from './data/courses.json';
import CoursesList from './components/CoursesList.jsx';
import Hero from "./components/Hero.jsx";

function App() {

    return (
        <>
            <main className="main">
                <Hero />
                <CoursesList courses={courses} />
            </main>
        </>
    );
}

export default App;
